package com.lesfermiers.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.lesfermiers.IntegrationTest;
import com.lesfermiers.domain.Campagne;
import com.lesfermiers.repository.CampagneRepository;
import com.lesfermiers.service.dto.CampagneDTO;
import com.lesfermiers.service.mapper.CampagneMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CampagneResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CampagneResourceIT {

    private static final Long DEFAULT_PRIX = 1L;
    private static final Long UPDATED_PRIX = 2L;

    private static final String DEFAULT_PRIX_DETAILLE = "AAAAAAAAAA";
    private static final String UPDATED_PRIX_DETAILLE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_LIVRAISON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_LIVRAISON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_VIDEO_URL = "AAAAAAAAAA";
    private static final String UPDATED_VIDEO_URL = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_FIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_FIN = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_PRIORITE = 1;
    private static final Integer UPDATED_PRIORITE = 2;

    private static final String ENTITY_API_URL = "/api/campagnes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CampagneRepository campagneRepository;

    @Autowired
    private CampagneMapper campagneMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCampagneMockMvc;

    private Campagne campagne;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Campagne createEntity(EntityManager em) {
        Campagne campagne = new Campagne()
            .prix(DEFAULT_PRIX)
            .prixDetaille(DEFAULT_PRIX_DETAILLE)
            .dateLivraison(DEFAULT_DATE_LIVRAISON)
            .description(DEFAULT_DESCRIPTION)
            .videoUrl(DEFAULT_VIDEO_URL)
            .dateFin(DEFAULT_DATE_FIN)
            .priorite(DEFAULT_PRIORITE);
        return campagne;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Campagne createUpdatedEntity(EntityManager em) {
        Campagne campagne = new Campagne()
            .prix(UPDATED_PRIX)
            .prixDetaille(UPDATED_PRIX_DETAILLE)
            .dateLivraison(UPDATED_DATE_LIVRAISON)
            .description(UPDATED_DESCRIPTION)
            .videoUrl(UPDATED_VIDEO_URL)
            .dateFin(UPDATED_DATE_FIN)
            .priorite(UPDATED_PRIORITE);
        return campagne;
    }

    @BeforeEach
    public void initTest() {
        campagne = createEntity(em);
    }

    @Test
    @Transactional
    void createCampagne() throws Exception {
        int databaseSizeBeforeCreate = campagneRepository.findAll().size();
        // Create the Campagne
        CampagneDTO campagneDTO = campagneMapper.toDto(campagne);
        restCampagneMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(campagneDTO)))
            .andExpect(status().isCreated());

        // Validate the Campagne in the database
        List<Campagne> campagneList = campagneRepository.findAll();
        assertThat(campagneList).hasSize(databaseSizeBeforeCreate + 1);
        Campagne testCampagne = campagneList.get(campagneList.size() - 1);
        assertThat(testCampagne.getPrix()).isEqualTo(DEFAULT_PRIX);
        assertThat(testCampagne.getPrixDetaille()).isEqualTo(DEFAULT_PRIX_DETAILLE);
        assertThat(testCampagne.getDateLivraison()).isEqualTo(DEFAULT_DATE_LIVRAISON);
        assertThat(testCampagne.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testCampagne.getVideoUrl()).isEqualTo(DEFAULT_VIDEO_URL);
        assertThat(testCampagne.getDateFin()).isEqualTo(DEFAULT_DATE_FIN);
        assertThat(testCampagne.getPriorite()).isEqualTo(DEFAULT_PRIORITE);
    }

    @Test
    @Transactional
    void createCampagneWithExistingId() throws Exception {
        // Create the Campagne with an existing ID
        campagne.setId(1L);
        CampagneDTO campagneDTO = campagneMapper.toDto(campagne);

        int databaseSizeBeforeCreate = campagneRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCampagneMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(campagneDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Campagne in the database
        List<Campagne> campagneList = campagneRepository.findAll();
        assertThat(campagneList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllCampagnes() throws Exception {
        // Initialize the database
        campagneRepository.saveAndFlush(campagne);

        // Get all the campagneList
        restCampagneMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(campagne.getId().intValue())))
            .andExpect(jsonPath("$.[*].prix").value(hasItem(DEFAULT_PRIX.intValue())))
            .andExpect(jsonPath("$.[*].prixDetaille").value(hasItem(DEFAULT_PRIX_DETAILLE)))
            .andExpect(jsonPath("$.[*].dateLivraison").value(hasItem(DEFAULT_DATE_LIVRAISON.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].videoUrl").value(hasItem(DEFAULT_VIDEO_URL)))
            .andExpect(jsonPath("$.[*].dateFin").value(hasItem(DEFAULT_DATE_FIN.toString())))
            .andExpect(jsonPath("$.[*].priorite").value(hasItem(DEFAULT_PRIORITE)));
    }

    @Test
    @Transactional
    void getCampagne() throws Exception {
        // Initialize the database
        campagneRepository.saveAndFlush(campagne);

        // Get the campagne
        restCampagneMockMvc
            .perform(get(ENTITY_API_URL_ID, campagne.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(campagne.getId().intValue()))
            .andExpect(jsonPath("$.prix").value(DEFAULT_PRIX.intValue()))
            .andExpect(jsonPath("$.prixDetaille").value(DEFAULT_PRIX_DETAILLE))
            .andExpect(jsonPath("$.dateLivraison").value(DEFAULT_DATE_LIVRAISON.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.videoUrl").value(DEFAULT_VIDEO_URL))
            .andExpect(jsonPath("$.dateFin").value(DEFAULT_DATE_FIN.toString()))
            .andExpect(jsonPath("$.priorite").value(DEFAULT_PRIORITE));
    }

    @Test
    @Transactional
    void getNonExistingCampagne() throws Exception {
        // Get the campagne
        restCampagneMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCampagne() throws Exception {
        // Initialize the database
        campagneRepository.saveAndFlush(campagne);

        int databaseSizeBeforeUpdate = campagneRepository.findAll().size();

        // Update the campagne
        Campagne updatedCampagne = campagneRepository.findById(campagne.getId()).get();
        // Disconnect from session so that the updates on updatedCampagne are not directly saved in db
        em.detach(updatedCampagne);
        updatedCampagne
            .prix(UPDATED_PRIX)
            .prixDetaille(UPDATED_PRIX_DETAILLE)
            .dateLivraison(UPDATED_DATE_LIVRAISON)
            .description(UPDATED_DESCRIPTION)
            .videoUrl(UPDATED_VIDEO_URL)
            .dateFin(UPDATED_DATE_FIN)
            .priorite(UPDATED_PRIORITE);
        CampagneDTO campagneDTO = campagneMapper.toDto(updatedCampagne);

        restCampagneMockMvc
            .perform(
                put(ENTITY_API_URL_ID, campagneDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(campagneDTO))
            )
            .andExpect(status().isOk());

        // Validate the Campagne in the database
        List<Campagne> campagneList = campagneRepository.findAll();
        assertThat(campagneList).hasSize(databaseSizeBeforeUpdate);
        Campagne testCampagne = campagneList.get(campagneList.size() - 1);
        assertThat(testCampagne.getPrix()).isEqualTo(UPDATED_PRIX);
        assertThat(testCampagne.getPrixDetaille()).isEqualTo(UPDATED_PRIX_DETAILLE);
        assertThat(testCampagne.getDateLivraison()).isEqualTo(UPDATED_DATE_LIVRAISON);
        assertThat(testCampagne.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testCampagne.getVideoUrl()).isEqualTo(UPDATED_VIDEO_URL);
        assertThat(testCampagne.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
        assertThat(testCampagne.getPriorite()).isEqualTo(UPDATED_PRIORITE);
    }

    @Test
    @Transactional
    void putNonExistingCampagne() throws Exception {
        int databaseSizeBeforeUpdate = campagneRepository.findAll().size();
        campagne.setId(count.incrementAndGet());

        // Create the Campagne
        CampagneDTO campagneDTO = campagneMapper.toDto(campagne);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCampagneMockMvc
            .perform(
                put(ENTITY_API_URL_ID, campagneDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(campagneDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Campagne in the database
        List<Campagne> campagneList = campagneRepository.findAll();
        assertThat(campagneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCampagne() throws Exception {
        int databaseSizeBeforeUpdate = campagneRepository.findAll().size();
        campagne.setId(count.incrementAndGet());

        // Create the Campagne
        CampagneDTO campagneDTO = campagneMapper.toDto(campagne);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCampagneMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(campagneDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Campagne in the database
        List<Campagne> campagneList = campagneRepository.findAll();
        assertThat(campagneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCampagne() throws Exception {
        int databaseSizeBeforeUpdate = campagneRepository.findAll().size();
        campagne.setId(count.incrementAndGet());

        // Create the Campagne
        CampagneDTO campagneDTO = campagneMapper.toDto(campagne);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCampagneMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(campagneDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Campagne in the database
        List<Campagne> campagneList = campagneRepository.findAll();
        assertThat(campagneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCampagneWithPatch() throws Exception {
        // Initialize the database
        campagneRepository.saveAndFlush(campagne);

        int databaseSizeBeforeUpdate = campagneRepository.findAll().size();

        // Update the campagne using partial update
        Campagne partialUpdatedCampagne = new Campagne();
        partialUpdatedCampagne.setId(campagne.getId());

        partialUpdatedCampagne.prix(UPDATED_PRIX).dateLivraison(UPDATED_DATE_LIVRAISON).description(UPDATED_DESCRIPTION);

        restCampagneMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCampagne.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCampagne))
            )
            .andExpect(status().isOk());

        // Validate the Campagne in the database
        List<Campagne> campagneList = campagneRepository.findAll();
        assertThat(campagneList).hasSize(databaseSizeBeforeUpdate);
        Campagne testCampagne = campagneList.get(campagneList.size() - 1);
        assertThat(testCampagne.getPrix()).isEqualTo(UPDATED_PRIX);
        assertThat(testCampagne.getPrixDetaille()).isEqualTo(DEFAULT_PRIX_DETAILLE);
        assertThat(testCampagne.getDateLivraison()).isEqualTo(UPDATED_DATE_LIVRAISON);
        assertThat(testCampagne.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testCampagne.getVideoUrl()).isEqualTo(DEFAULT_VIDEO_URL);
        assertThat(testCampagne.getDateFin()).isEqualTo(DEFAULT_DATE_FIN);
        assertThat(testCampagne.getPriorite()).isEqualTo(DEFAULT_PRIORITE);
    }

    @Test
    @Transactional
    void fullUpdateCampagneWithPatch() throws Exception {
        // Initialize the database
        campagneRepository.saveAndFlush(campagne);

        int databaseSizeBeforeUpdate = campagneRepository.findAll().size();

        // Update the campagne using partial update
        Campagne partialUpdatedCampagne = new Campagne();
        partialUpdatedCampagne.setId(campagne.getId());

        partialUpdatedCampagne
            .prix(UPDATED_PRIX)
            .prixDetaille(UPDATED_PRIX_DETAILLE)
            .dateLivraison(UPDATED_DATE_LIVRAISON)
            .description(UPDATED_DESCRIPTION)
            .videoUrl(UPDATED_VIDEO_URL)
            .dateFin(UPDATED_DATE_FIN)
            .priorite(UPDATED_PRIORITE);

        restCampagneMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCampagne.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCampagne))
            )
            .andExpect(status().isOk());

        // Validate the Campagne in the database
        List<Campagne> campagneList = campagneRepository.findAll();
        assertThat(campagneList).hasSize(databaseSizeBeforeUpdate);
        Campagne testCampagne = campagneList.get(campagneList.size() - 1);
        assertThat(testCampagne.getPrix()).isEqualTo(UPDATED_PRIX);
        assertThat(testCampagne.getPrixDetaille()).isEqualTo(UPDATED_PRIX_DETAILLE);
        assertThat(testCampagne.getDateLivraison()).isEqualTo(UPDATED_DATE_LIVRAISON);
        assertThat(testCampagne.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testCampagne.getVideoUrl()).isEqualTo(UPDATED_VIDEO_URL);
        assertThat(testCampagne.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
        assertThat(testCampagne.getPriorite()).isEqualTo(UPDATED_PRIORITE);
    }

    @Test
    @Transactional
    void patchNonExistingCampagne() throws Exception {
        int databaseSizeBeforeUpdate = campagneRepository.findAll().size();
        campagne.setId(count.incrementAndGet());

        // Create the Campagne
        CampagneDTO campagneDTO = campagneMapper.toDto(campagne);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCampagneMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, campagneDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(campagneDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Campagne in the database
        List<Campagne> campagneList = campagneRepository.findAll();
        assertThat(campagneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCampagne() throws Exception {
        int databaseSizeBeforeUpdate = campagneRepository.findAll().size();
        campagne.setId(count.incrementAndGet());

        // Create the Campagne
        CampagneDTO campagneDTO = campagneMapper.toDto(campagne);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCampagneMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(campagneDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Campagne in the database
        List<Campagne> campagneList = campagneRepository.findAll();
        assertThat(campagneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCampagne() throws Exception {
        int databaseSizeBeforeUpdate = campagneRepository.findAll().size();
        campagne.setId(count.incrementAndGet());

        // Create the Campagne
        CampagneDTO campagneDTO = campagneMapper.toDto(campagne);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCampagneMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(campagneDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Campagne in the database
        List<Campagne> campagneList = campagneRepository.findAll();
        assertThat(campagneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCampagne() throws Exception {
        // Initialize the database
        campagneRepository.saveAndFlush(campagne);

        int databaseSizeBeforeDelete = campagneRepository.findAll().size();

        // Delete the campagne
        restCampagneMockMvc
            .perform(delete(ENTITY_API_URL_ID, campagne.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Campagne> campagneList = campagneRepository.findAll();
        assertThat(campagneList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
