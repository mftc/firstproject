import { ICampagne } from 'app/entities/campagne/campagne.model';

export interface IUtilisateur {
  id?: number;
  nom?: string | null;
  prenom?: string | null;
  email?: string | null;
  password?: string | null;
  societe?: string | null;
  presentation?: string | null;
  telephone?: number | null;
  videoUrl?: string | null;
  photoUrl?: string | null;
  campagnes?: ICampagne[] | null;
}

export class Utilisateur implements IUtilisateur {
  constructor(
    public id?: number,
    public nom?: string | null,
    public prenom?: string | null,
    public email?: string | null,
    public password?: string | null,
    public societe?: string | null,
    public presentation?: string | null,
    public telephone?: number | null,
    public videoUrl?: string | null,
    public photoUrl?: string | null,
    public campagnes?: ICampagne[] | null
  ) {}
}

export function getUtilisateurIdentifier(utilisateur: IUtilisateur): number | undefined {
  return utilisateur.id;
}
