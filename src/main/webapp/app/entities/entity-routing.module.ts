import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Authority } from 'app/config/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'campagne',
        data: { pageTitle: 'lesfermiersApp.campagne.home.title' },
        loadChildren: () => import('./campagne/campagne.module').then(m => m.CampagneModule),
      },
      {
        path: 'ville',
        canActivate: [UserRouteAccessService],
        data: { authorities: [Authority.ADMIN], pageTitle: 'lesfermiersApp.ville.home.title' },
        loadChildren: () => import('./ville/ville.module').then(m => m.VilleModule),
      },
      {
        path: 'utilisateur',
        canActivate: [UserRouteAccessService],
        data: { authorities: [Authority.ADMIN], pageTitle: 'lesfermiersApp.utilisateur.home.title' },
        loadChildren: () => import('./utilisateur/utilisateur.module').then(m => m.UtilisateurModule),
      },
      {
        path: 'produit',
        canActivate: [UserRouteAccessService],
        data: { authorities: [Authority.ADMIN], pageTitle: 'lesfermiersApp.produit.home.title' },
        loadChildren: () => import('./produit/produit.module').then(m => m.ProduitModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
