import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICampagne } from '../campagne.model';

import { ASC, DESC, ITEMS_PER_PAGE, SORT } from 'app/config/pagination.constants';
import { CampagneService } from '../service/campagne.service';
import { CampagneDeleteDialogComponent } from '../delete/campagne-delete-dialog.component';
import { Account } from 'app/core/auth/account.model';
import { AccountService } from 'app/core/auth/account.service';

@Component({
  selector: 'jhi-campagne',
  templateUrl: './campagne.component.html',
})
export class CampagneComponent implements OnInit {
  campagnes?: ICampagne[];
  isLoading = false;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page?: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  account: Account | null = null;
  constructor(
    protected campagneService: CampagneService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected modalService: NgbModal,
    protected _accountService: AccountService
  ) {}

  loadPage(page?: number, dontNavigate?: boolean): void {
    this.isLoading = true;
    const pageToLoad: number = page ?? this.page ?? 1;
    /* eslint-disable no-console */

    this.campagneService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe(
        (res: HttpResponse<ICampagne[]>) => {
          this.isLoading = false;
          console.log('resulat avant filtre', res.body);
          console.log(this.account);

          const campagneToDisplay: ICampagne[] | undefined = res.body?.filter(
            campagne =>
              campagne.utilisateur?.email === this.account?.email ||
              this.account?.authorities.find(role => role === 'ROLE_ADMIN') !== undefined
          );
          console.log('resultat apres filtre', campagneToDisplay);

          this.onSuccess(campagneToDisplay, res.headers, pageToLoad, !dontNavigate);
        },
        () => {
          this.isLoading = false;
          this.onError();
        }
      );
  }

  ngOnInit(): void {
    this.handleNavigation();
    this._accountService.getAuthenticationState().subscribe(account => (this.account = account));
    console.log(this._accountService.hasAnyAuthority('ROLE_ADMIN'));
    this._accountService.identity(true).subscribe((compte: Account | null) => {
      this.account = compte;
    });
    console.log(this.account);
  }

  trackId(index: number, item: ICampagne): number {
    return item.id!;
  }

  delete(campagne: ICampagne): void {
    const modalRef = this.modalService.open(CampagneDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.campagne = campagne;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadPage();
      }
    });
  }

  protected sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? ASC : DESC)];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected handleNavigation(): void {
    combineLatest([this.activatedRoute.data, this.activatedRoute.queryParamMap]).subscribe(([data, params]) => {
      const page = params.get('page');
      const pageNumber = +(page ?? 1);
      const sort = (params.get(SORT) ?? data['defaultSort']).split(',');
      const predicate = sort[0];
      const ascending = sort[1] === ASC;
      if (pageNumber !== this.page || predicate !== this.predicate || ascending !== this.ascending) {
        this.predicate = predicate;
        this.ascending = ascending;
        this.loadPage(pageNumber, true);
      }
    });
  }

  protected onSuccess(data: ICampagne[] | undefined, headers: HttpHeaders, page: number, navigate: boolean): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    if (navigate) {
      this.router.navigate(['/campagne'], {
        queryParams: {
          page: this.page,
          size: this.itemsPerPage,
          sort: this.predicate + ',' + (this.ascending ? ASC : DESC),
        },
      });
    }
    this.campagnes = data ?? [];
    this.ngbPaginationPage = this.page;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page ?? 1;
  }
}
