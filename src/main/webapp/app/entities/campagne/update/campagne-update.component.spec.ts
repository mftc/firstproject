jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { CampagneService } from '../service/campagne.service';
import { ICampagne, Campagne } from '../campagne.model';
import { IVille } from 'app/entities/ville/ville.model';
import { VilleService } from 'app/entities/ville/service/ville.service';
import { IUtilisateur } from 'app/entities/utilisateur/utilisateur.model';
import { UtilisateurService } from 'app/entities/utilisateur/service/utilisateur.service';
import { IProduit } from 'app/entities/produit/produit.model';
import { ProduitService } from 'app/entities/produit/service/produit.service';

import { CampagneUpdateComponent } from './campagne-update.component';

describe('Campagne Management Update Component', () => {
  let comp: CampagneUpdateComponent;
  let fixture: ComponentFixture<CampagneUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let campagneService: CampagneService;
  let villeService: VilleService;
  let utilisateurService: UtilisateurService;
  let produitService: ProduitService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [CampagneUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(CampagneUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CampagneUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    campagneService = TestBed.inject(CampagneService);
    villeService = TestBed.inject(VilleService);
    utilisateurService = TestBed.inject(UtilisateurService);
    produitService = TestBed.inject(ProduitService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Ville query and add missing value', () => {
      const campagne: ICampagne = { id: 456 };
      const ville: IVille = { id: 73492 };
      campagne.ville = ville;

      const villeCollection: IVille[] = [{ id: 81340 }];
      jest.spyOn(villeService, 'query').mockReturnValue(of(new HttpResponse({ body: villeCollection })));
      const additionalVilles = [ville];
      const expectedCollection: IVille[] = [...additionalVilles, ...villeCollection];
      jest.spyOn(villeService, 'addVilleToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ campagne });
      comp.ngOnInit();

      expect(villeService.query).toHaveBeenCalled();
      expect(villeService.addVilleToCollectionIfMissing).toHaveBeenCalledWith(villeCollection, ...additionalVilles);
      expect(comp.villesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Utilisateur query and add missing value', () => {
      const campagne: ICampagne = { id: 456 };
      const utilisateur: IUtilisateur = { id: 57425 };
      campagne.utilisateur = utilisateur;

      const utilisateurCollection: IUtilisateur[] = [{ id: 82551 }];
      jest.spyOn(utilisateurService, 'query').mockReturnValue(of(new HttpResponse({ body: utilisateurCollection })));
      const additionalUtilisateurs = [utilisateur];
      const expectedCollection: IUtilisateur[] = [...additionalUtilisateurs, ...utilisateurCollection];
      jest.spyOn(utilisateurService, 'addUtilisateurToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ campagne });
      comp.ngOnInit();

      expect(utilisateurService.query).toHaveBeenCalled();
      expect(utilisateurService.addUtilisateurToCollectionIfMissing).toHaveBeenCalledWith(utilisateurCollection, ...additionalUtilisateurs);
      expect(comp.utilisateursSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Produit query and add missing value', () => {
      const campagne: ICampagne = { id: 456 };
      const produit: IProduit = { id: 97423 };
      campagne.produit = produit;

      const produitCollection: IProduit[] = [{ id: 3832 }];
      jest.spyOn(produitService, 'query').mockReturnValue(of(new HttpResponse({ body: produitCollection })));
      const additionalProduits = [produit];
      const expectedCollection: IProduit[] = [...additionalProduits, ...produitCollection];
      jest.spyOn(produitService, 'addProduitToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ campagne });
      comp.ngOnInit();

      expect(produitService.query).toHaveBeenCalled();
      expect(produitService.addProduitToCollectionIfMissing).toHaveBeenCalledWith(produitCollection, ...additionalProduits);
      expect(comp.produitsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const campagne: ICampagne = { id: 456 };
      const ville: IVille = { id: 10414 };
      campagne.ville = ville;
      const utilisateur: IUtilisateur = { id: 35584 };
      campagne.utilisateur = utilisateur;
      const produit: IProduit = { id: 91180 };
      campagne.produit = produit;

      activatedRoute.data = of({ campagne });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(campagne));
      expect(comp.villesSharedCollection).toContain(ville);
      expect(comp.utilisateursSharedCollection).toContain(utilisateur);
      expect(comp.produitsSharedCollection).toContain(produit);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Campagne>>();
      const campagne = { id: 123 };
      jest.spyOn(campagneService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ campagne });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: campagne }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(campagneService.update).toHaveBeenCalledWith(campagne);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Campagne>>();
      const campagne = new Campagne();
      jest.spyOn(campagneService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ campagne });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: campagne }));
      saveSubject.complete();

      // THEN
      expect(campagneService.create).toHaveBeenCalledWith(campagne);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Campagne>>();
      const campagne = { id: 123 };
      jest.spyOn(campagneService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ campagne });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(campagneService.update).toHaveBeenCalledWith(campagne);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackVilleById', () => {
      it('Should return tracked Ville primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackVilleById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackUtilisateurById', () => {
      it('Should return tracked Utilisateur primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackUtilisateurById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackProduitById', () => {
      it('Should return tracked Produit primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackProduitById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
