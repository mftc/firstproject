import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ICampagne, Campagne } from '../campagne.model';
import { CampagneService } from '../service/campagne.service';
import { IVille } from 'app/entities/ville/ville.model';
import { VilleService } from 'app/entities/ville/service/ville.service';
import { IUtilisateur } from 'app/entities/utilisateur/utilisateur.model';
import { UtilisateurService } from 'app/entities/utilisateur/service/utilisateur.service';
import { IProduit } from 'app/entities/produit/produit.model';
import { ProduitService } from 'app/entities/produit/service/produit.service';

@Component({
  selector: 'jhi-campagne-update',
  templateUrl: './campagne-update.component.html',
})
export class CampagneUpdateComponent implements OnInit {
  isSaving = false;

  villesSharedCollection: IVille[] = [];
  utilisateursSharedCollection: IUtilisateur[] = [];
  produitsSharedCollection: IProduit[] = [];

  editForm = this.fb.group({
    id: [],
    prix: [],
    prixDetaille: [],
    dateLivraison: [],
    description: [],
    videoUrl: [],
    dateFin: [],
    priorite: [],
    adresse: [],
    dateCreation: [],
    ville: [],
    utilisateur: [],
    produit: [],
  });

  constructor(
    protected campagneService: CampagneService,
    protected villeService: VilleService,
    protected utilisateurService: UtilisateurService,
    protected produitService: ProduitService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ campagne }) => {
      this.updateForm(campagne);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const campagne = this.createFromForm();
    if (campagne.id !== undefined) {
      this.subscribeToSaveResponse(this.campagneService.update(campagne));
    } else {
      this.subscribeToSaveResponse(this.campagneService.create(campagne));
    }
  }

  trackVilleById(index: number, item: IVille): number {
    return item.id!;
  }

  trackUtilisateurById(index: number, item: IUtilisateur): number {
    return item.id!;
  }

  trackProduitById(index: number, item: IProduit): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICampagne>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(campagne: ICampagne): void {
    this.editForm.patchValue({
      id: campagne.id,
      prix: campagne.prix,
      prixDetaille: campagne.prixDetaille,
      dateLivraison: campagne.dateLivraison,
      description: campagne.description,
      videoUrl: campagne.videoUrl,
      dateFin: campagne.dateFin,
      priorite: campagne.priorite,
      adresse: campagne.adresse,
      dateCreation: campagne.dateCreation,
      ville: campagne.ville,
      utilisateur: campagne.utilisateur,
      produit: campagne.produit,
    });

    this.villesSharedCollection = this.villeService.addVilleToCollectionIfMissing(this.villesSharedCollection, campagne.ville);
    this.utilisateursSharedCollection = this.utilisateurService.addUtilisateurToCollectionIfMissing(
      this.utilisateursSharedCollection,
      campagne.utilisateur
    );
    this.produitsSharedCollection = this.produitService.addProduitToCollectionIfMissing(this.produitsSharedCollection, campagne.produit);
  }

  protected loadRelationshipsOptions(): void {
    this.villeService
      .query()
      .pipe(map((res: HttpResponse<IVille[]>) => res.body ?? []))
      .pipe(map((villes: IVille[]) => this.villeService.addVilleToCollectionIfMissing(villes, this.editForm.get('ville')!.value)))
      .subscribe((villes: IVille[]) => (this.villesSharedCollection = villes));

    this.utilisateurService
      .query()
      .pipe(map((res: HttpResponse<IUtilisateur[]>) => res.body ?? []))
      .pipe(
        map((utilisateurs: IUtilisateur[]) =>
          this.utilisateurService.addUtilisateurToCollectionIfMissing(utilisateurs, this.editForm.get('utilisateur')!.value)
        )
      )
      .subscribe((utilisateurs: IUtilisateur[]) => (this.utilisateursSharedCollection = utilisateurs));

    this.produitService
      .query()
      .pipe(map((res: HttpResponse<IProduit[]>) => res.body ?? []))
      .pipe(
        map((produits: IProduit[]) => this.produitService.addProduitToCollectionIfMissing(produits, this.editForm.get('produit')!.value))
      )
      .subscribe((produits: IProduit[]) => (this.produitsSharedCollection = produits));
  }

  protected createFromForm(): ICampagne {
    const user = this.editForm.get(['utilisateur'])!.value
      ? this.editForm.get(['utilisateur'])!.value
      : JSON.parse(localStorage.getItem('connectedUser')!);

    return {
      ...new Campagne(),
      id: this.editForm.get(['id'])!.value,
      prix: this.editForm.get(['prix'])!.value,
      prixDetaille: this.editForm.get(['prixDetaille'])!.value,
      dateLivraison: this.editForm.get(['dateLivraison'])!.value,
      description: this.editForm.get(['description'])!.value,
      videoUrl: this.editForm.get(['videoUrl'])!.value,
      dateFin: this.editForm.get(['dateFin'])!.value,
      priorite: this.editForm.get(['priorite'])!.value,
      adresse: this.editForm.get(['adresse'])!.value,
      dateCreation: this.editForm.get(['dateCreation'])!.value,
      ville: this.editForm.get(['ville'])!.value,
      utilisateur: this.editForm.get(['utilisateur'])!.value,
      produit: this.editForm.get(['produit'])!.value,
    };
  }
}
