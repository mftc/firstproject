jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { ICampagne, Campagne } from '../campagne.model';
import { CampagneService } from '../service/campagne.service';

import { CampagneRoutingResolveService } from './campagne-routing-resolve.service';

describe('Campagne routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: CampagneRoutingResolveService;
  let service: CampagneService;
  let resultCampagne: ICampagne | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(CampagneRoutingResolveService);
    service = TestBed.inject(CampagneService);
    resultCampagne = undefined;
  });

  describe('resolve', () => {
    it('should return ICampagne returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCampagne = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultCampagne).toEqual({ id: 123 });
    });

    it('should return new ICampagne if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCampagne = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultCampagne).toEqual(new Campagne());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as Campagne })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCampagne = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultCampagne).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
