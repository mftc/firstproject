import * as dayjs from 'dayjs';
import { IVille } from 'app/entities/ville/ville.model';
import { IUtilisateur } from 'app/entities/utilisateur/utilisateur.model';
import { IProduit } from 'app/entities/produit/produit.model';
import { Etat } from 'app/enum/etat';

export interface ICampagne {
  id?: number;
  prix?: number | null;
  prixDetaille?: string | null;
  dateLivraison?: dayjs.Dayjs | null;
  description?: string | null;
  videoUrl?: string | null;
  dateFin?: dayjs.Dayjs | null;
  priorite?: number | null;
  adresse?: string | null;
  dateCreation?: dayjs.Dayjs | null;
  ville?: IVille | null;
  utilisateur?: IUtilisateur | null;
  produit?: IProduit | null;
  etat?: Etat | null;
}

export class Campagne implements ICampagne {
  constructor(
    public id?: number,
    public prix?: number | null,
    public prixDetaille?: string | null,
    public dateLivraison?: dayjs.Dayjs | null,
    public description?: string | null,
    public videoUrl?: string | null,
    public dateFin?: dayjs.Dayjs | null,
    public priorite?: number | null,
    public adresse?: string | null,
    public dateCreation?: dayjs.Dayjs | null,
    public ville?: IVille | null,
    public utilisateur?: IUtilisateur | null,
    public produit?: IProduit | null
  ) {}
}

export function getCampagneIdentifier(campagne: ICampagne): number | undefined {
  return campagne.id;
}
