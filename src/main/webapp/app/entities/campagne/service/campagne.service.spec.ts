import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_FORMAT } from 'app/config/input.constants';
import { ICampagne, Campagne } from '../campagne.model';

import { CampagneService } from './campagne.service';

describe('Campagne Service', () => {
  let service: CampagneService;
  let httpMock: HttpTestingController;
  let elemDefault: ICampagne;
  let expectedResult: ICampagne | ICampagne[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(CampagneService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      prix: 0,
      prixDetaille: 'AAAAAAA',
      dateLivraison: currentDate,
      description: 'AAAAAAA',
      videoUrl: 'AAAAAAA',
      dateFin: currentDate,
      priorite: 0,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          dateLivraison: currentDate.format(DATE_FORMAT),
          dateFin: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Campagne', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          dateLivraison: currentDate.format(DATE_FORMAT),
          dateFin: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateLivraison: currentDate,
          dateFin: currentDate,
        },
        returnedFromService
      );

      service.create(new Campagne()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Campagne', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          prix: 1,
          prixDetaille: 'BBBBBB',
          dateLivraison: currentDate.format(DATE_FORMAT),
          description: 'BBBBBB',
          videoUrl: 'BBBBBB',
          dateFin: currentDate.format(DATE_FORMAT),
          priorite: 1,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateLivraison: currentDate,
          dateFin: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Campagne', () => {
      const patchObject = Object.assign(
        {
          prix: 1,
          dateLivraison: currentDate.format(DATE_FORMAT),
          description: 'BBBBBB',
          priorite: 1,
        },
        new Campagne()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          dateLivraison: currentDate,
          dateFin: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Campagne', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          prix: 1,
          prixDetaille: 'BBBBBB',
          dateLivraison: currentDate.format(DATE_FORMAT),
          description: 'BBBBBB',
          videoUrl: 'BBBBBB',
          dateFin: currentDate.format(DATE_FORMAT),
          priorite: 1,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateLivraison: currentDate,
          dateFin: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Campagne', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addCampagneToCollectionIfMissing', () => {
      it('should add a Campagne to an empty array', () => {
        const campagne: ICampagne = { id: 123 };
        expectedResult = service.addCampagneToCollectionIfMissing([], campagne);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(campagne);
      });

      it('should not add a Campagne to an array that contains it', () => {
        const campagne: ICampagne = { id: 123 };
        const campagneCollection: ICampagne[] = [
          {
            ...campagne,
          },
          { id: 456 },
        ];
        expectedResult = service.addCampagneToCollectionIfMissing(campagneCollection, campagne);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Campagne to an array that doesn't contain it", () => {
        const campagne: ICampagne = { id: 123 };
        const campagneCollection: ICampagne[] = [{ id: 456 }];
        expectedResult = service.addCampagneToCollectionIfMissing(campagneCollection, campagne);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(campagne);
      });

      it('should add only unique Campagne to an array', () => {
        const campagneArray: ICampagne[] = [{ id: 123 }, { id: 456 }, { id: 1345 }];
        const campagneCollection: ICampagne[] = [{ id: 123 }];
        expectedResult = service.addCampagneToCollectionIfMissing(campagneCollection, ...campagneArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const campagne: ICampagne = { id: 123 };
        const campagne2: ICampagne = { id: 456 };
        expectedResult = service.addCampagneToCollectionIfMissing([], campagne, campagne2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(campagne);
        expect(expectedResult).toContain(campagne2);
      });

      it('should accept null and undefined values', () => {
        const campagne: ICampagne = { id: 123 };
        expectedResult = service.addCampagneToCollectionIfMissing([], null, campagne, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(campagne);
      });

      it('should return initial array if no Campagne is added', () => {
        const campagneCollection: ICampagne[] = [{ id: 123 }];
        expectedResult = service.addCampagneToCollectionIfMissing(campagneCollection, undefined, null);
        expect(expectedResult).toEqual(campagneCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
