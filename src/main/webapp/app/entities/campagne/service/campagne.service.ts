import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICampagne, getCampagneIdentifier } from '../campagne.model';

export type EntityResponseType = HttpResponse<ICampagne>;
export type EntityArrayResponseType = HttpResponse<ICampagne[]>;

@Injectable({ providedIn: 'root' })
export class CampagneService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/campagnes');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(campagne: ICampagne): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(campagne);
    return this.http
      .post<ICampagne>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(campagne: ICampagne): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(campagne);
    return this.http
      .put<ICampagne>(`${this.resourceUrl}/${getCampagneIdentifier(campagne) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(campagne: ICampagne): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(campagne);
    return this.http
      .patch<ICampagne>(`${this.resourceUrl}/${getCampagneIdentifier(campagne) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ICampagne>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICampagne[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCampagneToCollectionIfMissing(campagneCollection: ICampagne[], ...campagnesToCheck: (ICampagne | null | undefined)[]): ICampagne[] {
    const campagnes: ICampagne[] = campagnesToCheck.filter(isPresent);
    if (campagnes.length > 0) {
      const campagneCollectionIdentifiers = campagneCollection.map(campagneItem => getCampagneIdentifier(campagneItem)!);
      const campagnesToAdd = campagnes.filter(campagneItem => {
        const campagneIdentifier = getCampagneIdentifier(campagneItem);
        if (campagneIdentifier == null || campagneCollectionIdentifiers.includes(campagneIdentifier)) {
          return false;
        }
        campagneCollectionIdentifiers.push(campagneIdentifier);
        return true;
      });
      return [...campagnesToAdd, ...campagneCollection];
    }
    return campagneCollection;
  }

  // ===========================My Code ========================
  findAllCampagneFiltered(region: string, idVille: number, idProduit: number): Observable<EntityArrayResponseType> {
    return this.http
      .get<ICampagne[]>(`${this.resourceUrl}/filtered/${region}/${idVille}/${idProduit}`, { observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  findAllCampagneByUSer(idUtilisateur: number): Observable<EntityArrayResponseType> {
    return this.http
      .get<ICampagne[]>(`${this.resourceUrl}/user/${idUtilisateur}`, { observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  findCampagneForHome(): Observable<EntityArrayResponseType> {
    return this.http
      .get<ICampagne[]>(`${this.resourceUrl}/accueil`, { observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }
  // ============================ End my code ===================
  protected convertDateFromClient(campagne: ICampagne): ICampagne {
    return Object.assign({}, campagne, {
      dateLivraison: campagne.dateLivraison?.isValid() ? campagne.dateLivraison.format(DATE_FORMAT) : undefined,
      dateFin: campagne.dateFin?.isValid() ? campagne.dateFin.format(DATE_FORMAT) : undefined,
      dateCreation: campagne.dateCreation?.isValid() ? campagne.dateCreation.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateLivraison = res.body.dateLivraison ? dayjs(res.body.dateLivraison) : undefined;
      res.body.dateFin = res.body.dateFin ? dayjs(res.body.dateFin) : undefined;
      res.body.dateCreation = res.body.dateCreation ? dayjs(res.body.dateCreation) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((campagne: ICampagne) => {
        campagne.dateLivraison = campagne.dateLivraison ? dayjs(campagne.dateLivraison) : undefined;
        campagne.dateFin = campagne.dateFin ? dayjs(campagne.dateFin) : undefined;
        campagne.dateCreation = campagne.dateCreation ? dayjs(campagne.dateCreation) : undefined;
      });
    }
    return res;
  }
}
