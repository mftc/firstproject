import { ICampagne } from 'app/entities/campagne/campagne.model';

export interface IProduit {
  id?: number;
  libelle?: string | null;
  categorie?: string | null;
  campagnes?: ICampagne[] | null;
}

export class Produit implements IProduit {
  constructor(
    public id?: number,
    public libelle?: string | null,
    public categorie?: string | null,
    public campagnes?: ICampagne[] | null
  ) {}
}

export function getProduitIdentifier(produit: IProduit): number | undefined {
  return produit.id;
}
