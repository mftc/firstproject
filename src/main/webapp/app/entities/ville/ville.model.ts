import { ICampagne } from 'app/entities/campagne/campagne.model';

export interface IVille {
  id?: number;
  region?: string;
  libelle?: string;
  campagnes?: ICampagne[] | null;
}

export class Ville implements IVille {
  constructor(public id?: number, public region?: string, public libelle?: string, public campagnes?: ICampagne[] | null) {}
}

export function getVilleIdentifier(ville: IVille): number | undefined {
  return ville.id;
}
