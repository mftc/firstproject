import { HttpResponse } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Produit } from 'app/entities/produit/produit.model';
import { ProduitService } from 'app/entities/produit/service/produit.service';
import { UtilisateurService } from 'app/entities/utilisateur/service/utilisateur.service';
import { VilleService } from 'app/entities/ville/service/ville.service';
import { IVille, Ville } from 'app/entities/ville/ville.model';

@Component({
  selector: 'jhi-liste-campagnes-recherche',
  templateUrl: './liste-campagnes-recherche.component.html',
  styleUrls: ['./liste-campagnes-recherche.component.scss'],
})
export class ListeCampagnesRechercheComponent implements OnInit {
  listeRegion: Array<string> = ['Ouest', 'Centre', 'Sud', 'Est', 'Lithoral', 'Nord'];
  listeVille!: Array<Ville>;
  villes!: Array<Ville>;
  listeProduit!: Array<Produit>;

  @ViewChild('searchForm') ngForm!: NgForm;
  region!: string;
  ville!: Ville;
  produit!: Produit;
  isLoading!: boolean;
  @Output() sendParamProduit = new EventEmitter<Produit>();
  @Output() sendParam = new EventEmitter<any>();
  constructor(private _util: UtilisateurService, private _produitService: ProduitService, private _villeService: VilleService) {}

  ngOnInit(): void {
    this._villeService
      .query({
        page: -1,
        size: 20,
      })
      .subscribe(
        (res: HttpResponse<IVille[]>) => {
          this.isLoading = false;
          this.villes = res.body ?? [];
        },
        () => {
          this.isLoading = false;
          this.onError();
        }
      );
    //this.listeProduit = this._produitService.listeProduits;
    //this._produitService.emitListeProduits();
  }

  onSubmit(): void {
    if (this.ngForm.valid) {
      this.sendParam.emit({ region: this.region, ville: this.ville, produit: this.produit });
    }
  }
  majVille(region: any): void {
    //this.villes = this._util.getVilles(region);
  }

  onError(): void {
    // console.log('message erreur')
  }
}
