import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/auth/account.model';
import { LANGUAGES } from 'app/config/language.constants';
import { UtilisateurService } from 'app/entities/utilisateur/service/utilisateur.service';
import { IUtilisateur } from 'app/entities/utilisateur/utilisateur.model';

@Component({
  selector: 'jhi-settings',
  templateUrl: './settings.component.html',
})
export class SettingsComponent implements OnInit {
  account!: Account;
  success = false;
  languages = LANGUAGES;
  settingsForm = this.fb.group({
    firstName: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
    lastName: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
    societe: [undefined],
    presentation: [undefined],
    telephone: [undefined, [Validators.maxLength(8)]],
    videoUrl: [undefined],
  });

  constructor(
    private accountService: AccountService,
    private fb: FormBuilder,
    private translateService: TranslateService,
    private _utilisateurService: UtilisateurService
  ) {}

  ngOnInit(): void {
    this.accountService.identity().subscribe(account => {
      if (account) {
        this.settingsForm.patchValue({
          firstName: account.firstName,
          lastName: account.lastName,
          email: account.email,
        });

        this.account = account;
      }
    });
  }

  save(): void {
    this.success = false;

    this.account.firstName = this.settingsForm.get('firstName')!.value;
    this.account.lastName = this.settingsForm.get('lastName')!.value;
    /* eslint-disable no-console */
    console.log(localStorage.getItem('connectedUser'));
    const utilisateur: IUtilisateur = JSON.parse(localStorage.getItem('connectedUser')!);
    utilisateur.societe = this.settingsForm.get('societe')!.value;
    utilisateur.presentation = this.settingsForm.get('presentation')!.value;
    utilisateur.telephone = this.settingsForm.get('telephone')!.value;
    utilisateur.videoUrl = this.settingsForm.get('videoUrl')!.value;

    this.accountService.save(this.account).subscribe(() => {
      this.success = true;

      this.accountService.authenticate(this.account);

      if (this.account.langKey !== this.translateService.currentLang) {
        this.translateService.use(this.account.langKey);
      }
    });
    console.log("MAJ de l'utilisateur", utilisateur);

    this._utilisateurService.update(utilisateur).subscribe(() => {
      console.log('MAJ OK ===');
    });
  }
}
