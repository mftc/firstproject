import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';
import { ListeCampagnesRechercheComponent } from 'app/liste-campagnes-recherche/liste-campagnes-recherche.component';
import { RechercheComponent } from './recherche/recherche.component';
import { FournisseurComponent } from './fournisseur/fournisseur.component';

@NgModule({
  imports: [SharedModule, RouterModule.forChild(HOME_ROUTE)],
  declarations: [HomeComponent, ListeCampagnesRechercheComponent, RechercheComponent, FournisseurComponent],
})
export class HomeModule {}
