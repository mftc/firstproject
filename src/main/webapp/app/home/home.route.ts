import { Route } from '@angular/router';
import { FournisseurComponent } from './fournisseur/fournisseur.component';

import { HomeComponent } from './home.component';

export const HOME_ROUTE: Route[] = [
  {
    path: '',
    component: HomeComponent,
    data: {
      pageTitle: 'home.title',
    },
  },
  {
    path: 'fournisseur/:idUser',
    component: FournisseurComponent,
    data: {
      pageTitle: 'Detail du fournisseur',
    },
  },
];
