import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Campagne } from 'app/entities/campagne/campagne.model';
import { CampagneService } from 'app/entities/campagne/service/campagne.service';
import { EntityArrayResponseType, EntityResponseType, UtilisateurService } from 'app/entities/utilisateur/service/utilisateur.service';
import { IUtilisateur, Utilisateur } from 'app/entities/utilisateur/utilisateur.model';

@Component({
  selector: 'jhi-fournisseur',
  templateUrl: './fournisseur.component.html',
  styleUrls: ['./fournisseur.component.scss'],
})
export class FournisseurComponent implements OnInit {
  campagneList!: Campagne[] | null;
  userId!: number;
  fournisseur!: IUtilisateur | null;

  constructor(
    private _utilisateurService: UtilisateurService,
    private _campagneService: CampagneService,
    private _router: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.userId = +this._router.snapshot.paramMap.get('idUser')!;
    this._campagneService.findAllCampagneByUSer(this.userId).subscribe((campagnes: EntityArrayResponseType) => {
      this.campagneList = campagnes.body;
    });
    this._utilisateurService.find(this.userId).subscribe((user: EntityResponseType) => {
      this.fournisseur = user.body;
    });
  }
}
