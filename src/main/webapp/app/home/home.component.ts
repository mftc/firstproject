import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/auth/account.model';
import { CampagneService, EntityArrayResponseType } from 'app/entities/campagne/service/campagne.service';
import { ICampagne } from 'app/entities/campagne/campagne.model';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  account: Account | null = null;
  listeCampagne!: ICampagne[];
  private readonly destroy$ = new Subject<void>();

  constructor(private accountService: AccountService, private router: Router, private _campagneService: CampagneService) {}

  ngOnInit(): void {
    this.accountService
      .getAuthenticationState()
      .pipe(takeUntil(this.destroy$))
      .subscribe(account => (this.account = account));

    this._campagneService.findCampagneForHome().subscribe((campagnes: EntityArrayResponseType) => {
      this.listeCampagne = campagnes.body!;
    });
  }

  login(): void {
    this.router.navigate(['/login']);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
  search(param: any): void {
    /* eslint-disable no-console */
    console.log('liste des parametres de la requete ==> ', param);
    const idRegion = param?.region == null ? 'toutes' : param.region;
    const idVille = param?.ville == null ? 0 : param.ville.id;
    this._campagneService
      .findAllCampagneFiltered(idRegion, idVille, param?.produit?.id)
      .subscribe((campagneList: EntityArrayResponseType) => {
        this.listeCampagne = campagneList.body!;
      });
  }
}
