import { HttpResponse } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { IProduit, Produit } from 'app/entities/produit/produit.model';
import { ProduitService } from 'app/entities/produit/service/produit.service';
import { VilleService } from 'app/entities/ville/service/ville.service';
import { IVille, Ville } from 'app/entities/ville/ville.model';

@Component({
  selector: 'jhi-recherche',
  templateUrl: './recherche.component.html',
  styleUrls: ['./recherche.component.scss'],
})
export class RechercheComponent implements OnInit {
  listeRegion: Array<string> = ['Ouest', 'Centre', 'Sud', 'Est', 'Lithoral', 'Nord', 'Extrême-Nord', 'Adamaoua', 'Nord-Ouest', 'Sud-Ouest'];
  listeVille!: Array<IVille>;
  villes!: IVille[];
  listeProduit!: IProduit[];
  @Output() sendParam = new EventEmitter<any>();
  @ViewChild('searchForm') ngForm!: NgForm;
  region!: string;
  ville!: Ville;
  produit!: Produit;
  isLoading!: boolean;

  constructor(private _villeService: VilleService, private _produitService: ProduitService) {}

  ngOnInit(): void {
    /* eslint-disable no-console */
    // console.log("init recherche ");
    this._produitService.findAll().subscribe((p: HttpResponse<IProduit[]>) => {
      console.log('la liste de produit est ', p.body);
      this.listeProduit = p.body!;
      console.log('la liste de produit est ', p.body);
    });
  }
  onSubmit(): void {
    if (this.ngForm.valid) {
      console.log(this.ngForm.value);
      this.sendParam.emit({ region: this.region, ville: this.ville, produit: this.produit });
    }
  }

  majVille(region: string): void {
    let regionChoisie: string = region;
    if (!region) {
      regionChoisie = 'toutes';
    }
    console.log('la region est ', regionChoisie);

    this._villeService.villesByRegion(regionChoisie).subscribe((v: HttpResponse<IVille[]>) => {
      this.villes = v.body!;
      console.log(this.villes);
    });
  }
}
