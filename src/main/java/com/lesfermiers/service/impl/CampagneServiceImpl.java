package com.lesfermiers.service.impl;

import com.lesfermiers.domain.Campagne;
import com.lesfermiers.repository.CampagneRepository;
import com.lesfermiers.service.CampagneService;
import com.lesfermiers.service.dto.CampagneDTO;
import com.lesfermiers.service.mapper.CampagneMapper;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Campagne}.
 */
@Service
@Transactional
public class CampagneServiceImpl implements CampagneService {

    private final Logger log = LoggerFactory.getLogger(CampagneServiceImpl.class);

    private final CampagneRepository campagneRepository;

    private final CampagneMapper campagneMapper;

    public CampagneServiceImpl(CampagneRepository campagneRepository, CampagneMapper campagneMapper) {
        this.campagneRepository = campagneRepository;
        this.campagneMapper = campagneMapper;
    }

    @Override
    public CampagneDTO save(CampagneDTO campagneDTO) {
        log.debug("Request to save Campagne : {}", campagneDTO);
        Campagne campagne = campagneMapper.toEntity(campagneDTO);
        campagne = campagneRepository.save(campagne);
        return campagneMapper.toDto(campagne);
    }

    @Override
    public Optional<CampagneDTO> partialUpdate(CampagneDTO campagneDTO) {
        log.debug("Request to partially update Campagne : {}", campagneDTO);

        return campagneRepository
            .findById(campagneDTO.getId())
            .map(existingCampagne -> {
                campagneMapper.partialUpdate(existingCampagne, campagneDTO);

                return existingCampagne;
            })
            .map(campagneRepository::save)
            .map(campagneMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CampagneDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Campagnes");
        return campagneRepository.findAll(pageable).map(campagneMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CampagneDTO> findOne(Long id) {
        log.debug("Request to get Campagne : {}", id);
        return campagneRepository.findById(id).map(campagneMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Campagne : {}", id);
        campagneRepository.deleteById(id);
    }

    public List<CampagneDTO> findAllCampagneFiltered(String region, Long idville, Long idProduit) {
        log.debug("Request to get all Campagnes");
        if ("toutes".equals(region)) {
            return campagneRepository.findAllByProduit(idProduit).stream().map(c -> campagneMapper.toDto(c)).collect(Collectors.toList());
        } else if (idville == 0) {
            //rechercher la liste des campagnes par regionss
            return campagneRepository
                .findAllByRegion(region, idProduit)
                .stream()
                .map(c -> campagneMapper.toDto(c))
                .collect(Collectors.toList());
        } else {
            //rechercher la liste des campagnes par Ville
            return campagneRepository
                .findAllByVille(idville, idProduit)
                .stream()
                .map(c -> campagneMapper.toDto(c))
                .collect(Collectors.toList());
        }
    }

    /**
     * liste de campagnes par utilisateur
     * @param idUtilisateur
     * @return
     */
    @Override
    public List<CampagneDTO> findAllCampagneByUser(Long idUtilisateur) {
        log.debug(" recherche de toutes les campagnes par utilisateurs");
        return campagneRepository.findAllByUser(idUtilisateur).stream().map(c -> campagneMapper.toDto(c)).collect(Collectors.toList());
    }

    @Override
    public List<CampagneDTO> findCampagneForHome() {
        log.debug(" recherche des les campagnes pour la page d'accueil");
        return campagneRepository.findForHome().stream().map(c -> campagneMapper.toDto(c)).collect(Collectors.toList());
    }
}
