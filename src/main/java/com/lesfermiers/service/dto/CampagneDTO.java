package com.lesfermiers.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link com.lesfermiers.domain.Campagne} entity.
 */
public class CampagneDTO implements Serializable {

    private Long id;

    private Long prix;

    private String prixDetaille;

    private LocalDate dateLivraison;

    private String description;

    private String videoUrl;

    private LocalDate dateFin;

    private Integer priorite;

    private String adresse;

    private LocalDate dateCreation;

    private VilleDTO ville;

    private UtilisateurDTO utilisateur;

    private ProduitDTO produit;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPrix() {
        return prix;
    }

    public void setPrix(Long prix) {
        this.prix = prix;
    }

    public String getPrixDetaille() {
        return prixDetaille;
    }

    public void setPrixDetaille(String prixDetaille) {
        this.prixDetaille = prixDetaille;
    }

    public LocalDate getDateLivraison() {
        return dateLivraison;
    }

    public void setDateLivraison(LocalDate dateLivraison) {
        this.dateLivraison = dateLivraison;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public LocalDate getDateFin() {
        return dateFin;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }

    public Integer getPriorite() {
        return priorite;
    }

    public void setPriorite(Integer priorite) {
        this.priorite = priorite;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public LocalDate getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(LocalDate dateCreation) {
        this.dateCreation = dateCreation;
    }

    public VilleDTO getVille() {
        return ville;
    }

    public void setVille(VilleDTO ville) {
        this.ville = ville;
    }

    public UtilisateurDTO getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(UtilisateurDTO utilisateur) {
        this.utilisateur = utilisateur;
    }

    public ProduitDTO getProduit() {
        return produit;
    }

    public void setProduit(ProduitDTO produit) {
        this.produit = produit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CampagneDTO)) {
            return false;
        }

        CampagneDTO campagneDTO = (CampagneDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, campagneDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CampagneDTO{" +
            "id=" + getId() +
            ", prix=" + getPrix() +
            ", prixDetaille='" + getPrixDetaille() + "'" +
            ", dateLivraison='" + getDateLivraison() + "'" +
            ", description='" + getDescription() + "'" +
            ", videoUrl='" + getVideoUrl() + "'" +
            ", dateFin='" + getDateFin() + "'" +
            ", priorite=" + getPriorite() +
            ", adresse='" + getAdresse() + "'" +
            ", dateCreation='" + getDateCreation() + "'" +
            ", ville=" + getVille() +
            ", utilisateur=" + getUtilisateur() +
            ", produit=" + getProduit() +
            "}";
    }
}
