package com.lesfermiers.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.lesfermiers.domain.Ville} entity.
 */
public class VilleDTO implements Serializable {

    private Long id;

    @NotNull
    private String region;

    @NotNull
    private String libelle;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VilleDTO)) {
            return false;
        }

        VilleDTO villeDTO = (VilleDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, villeDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VilleDTO{" +
            "id=" + getId() +
            ", region='" + getRegion() + "'" +
            ", libelle='" + getLibelle() + "'" +
            "}";
    }
}
