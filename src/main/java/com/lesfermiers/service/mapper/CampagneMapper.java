package com.lesfermiers.service.mapper;

import com.lesfermiers.domain.Campagne;
import com.lesfermiers.service.dto.CampagneDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Campagne} and its DTO {@link CampagneDTO}.
 */
@Mapper(componentModel = "spring", uses = { VilleMapper.class, UtilisateurMapper.class, ProduitMapper.class })
public interface CampagneMapper extends EntityMapper<CampagneDTO, Campagne> {
    @Mapping(target = "ville", source = "ville")
    @Mapping(target = "utilisateur", source = "utilisateur")
    @Mapping(target = "produit", source = "produit")
    CampagneDTO toDto(Campagne s);
}
