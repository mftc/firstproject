package com.lesfermiers.repository;

import com.lesfermiers.domain.Campagne;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Campagne entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CampagneRepository extends JpaRepository<Campagne, Long> {
    @Query("From Campagne c where c.ville.region = :region and c.produit.id = :idProduit")
    List<Campagne> findAllByRegion(@Param("region") String region, @Param("idProduit") Long idProduit);

    @Query("From Campagne c where c.ville.id = :idVille and c.produit.id = :idProduit")
    List<Campagne> findAllByVille(@Param("idVille") Long idVille, @Param("idProduit") Long idProduit);

    @Query("From Campagne c where c.produit.id = :idProduit")
    List<Campagne> findAllByProduit(@Param("idProduit") Long idProduit);

    @Query("From Campagne c where c.utilisateur.id = :idUtilisateur")
    List<Campagne> findAllByUser(@Param("idUtilisateur") Long idUtilisateur);

    @Query("From Campagne c order by priorite asc , dateFin")
    List<Campagne> findForHome();
}
