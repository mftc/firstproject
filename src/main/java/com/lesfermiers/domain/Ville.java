package com.lesfermiers.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Ville.
 */
@Entity
@Table(name = "ville")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Ville implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "region", nullable = false)
    private String region;

    @NotNull
    @Column(name = "libelle", nullable = false)
    private String libelle;

    @OneToMany(mappedBy = "ville")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "ville", "utilisateur", "produit" }, allowSetters = true)
    private Set<Campagne> campagnes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Ville id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegion() {
        return this.region;
    }

    public Ville region(String region) {
        this.setRegion(region);
        return this;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getLibelle() {
        return this.libelle;
    }

    public Ville libelle(String libelle) {
        this.setLibelle(libelle);
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Set<Campagne> getCampagnes() {
        return this.campagnes;
    }

    public void setCampagnes(Set<Campagne> campagnes) {
        if (this.campagnes != null) {
            this.campagnes.forEach(i -> i.setVille(null));
        }
        if (campagnes != null) {
            campagnes.forEach(i -> i.setVille(this));
        }
        this.campagnes = campagnes;
    }

    public Ville campagnes(Set<Campagne> campagnes) {
        this.setCampagnes(campagnes);
        return this;
    }

    public Ville addCampagne(Campagne campagne) {
        this.campagnes.add(campagne);
        campagne.setVille(this);
        return this;
    }

    public Ville removeCampagne(Campagne campagne) {
        this.campagnes.remove(campagne);
        campagne.setVille(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Ville)) {
            return false;
        }
        return id != null && id.equals(((Ville) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Ville{" +
            "id=" + getId() +
            ", region='" + getRegion() + "'" +
            ", libelle='" + getLibelle() + "'" +
            "}";
    }
}
