package com.lesfermiers.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Produit.
 */
@Entity
@Table(name = "produit")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Produit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "categorie")
    private String categorie;

    @OneToMany(mappedBy = "produit")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "ville", "utilisateur", "produit" }, allowSetters = true)
    private Set<Campagne> campagnes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Produit id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return this.libelle;
    }

    public Produit libelle(String libelle) {
        this.setLibelle(libelle);
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getCategorie() {
        return this.categorie;
    }

    public Produit categorie(String categorie) {
        this.setCategorie(categorie);
        return this;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public Set<Campagne> getCampagnes() {
        return this.campagnes;
    }

    public void setCampagnes(Set<Campagne> campagnes) {
        if (this.campagnes != null) {
            this.campagnes.forEach(i -> i.setProduit(null));
        }
        if (campagnes != null) {
            campagnes.forEach(i -> i.setProduit(this));
        }
        this.campagnes = campagnes;
    }

    public Produit campagnes(Set<Campagne> campagnes) {
        this.setCampagnes(campagnes);
        return this;
    }

    public Produit addCampagne(Campagne campagne) {
        this.campagnes.add(campagne);
        campagne.setProduit(this);
        return this;
    }

    public Produit removeCampagne(Campagne campagne) {
        this.campagnes.remove(campagne);
        campagne.setProduit(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Produit)) {
            return false;
        }
        return id != null && id.equals(((Produit) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Produit{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", categorie='" + getCategorie() + "'" +
            "}";
    }
}
