package com.lesfermiers.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Utilisateur.
 */
@Entity
@Table(name = "utilisateur")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Utilisateur implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "prenom")
    private String prenom;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "societe")
    private String societe;

    @Column(name = "presentation")
    private String presentation;

    @Column(name = "telephone")
    private Long telephone;

    @Column(name = "video_url")
    private String videoUrl;

    @Column(name = "photo_url")
    private String photoUrl;

    @OneToMany(mappedBy = "utilisateur")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "ville", "utilisateur", "produit" }, allowSetters = true)
    private Set<Campagne> campagnes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Utilisateur id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public Utilisateur nom(String nom) {
        this.setNom(nom);
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public Utilisateur prenom(String prenom) {
        this.setPrenom(prenom);
        return this;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return this.email;
    }

    public Utilisateur email(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public Utilisateur password(String password) {
        this.setPassword(password);
        return this;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSociete() {
        return this.societe;
    }

    public Utilisateur societe(String societe) {
        this.setSociete(societe);
        return this;
    }

    public void setSociete(String societe) {
        this.societe = societe;
    }

    public String getPresentation() {
        return this.presentation;
    }

    public Utilisateur presentation(String presentation) {
        this.setPresentation(presentation);
        return this;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    public Long getTelephone() {
        return this.telephone;
    }

    public Utilisateur telephone(Long telephone) {
        this.setTelephone(telephone);
        return this;
    }

    public void setTelephone(Long telephone) {
        this.telephone = telephone;
    }

    public String getVideoUrl() {
        return this.videoUrl;
    }

    public Utilisateur videoUrl(String videoUrl) {
        this.setVideoUrl(videoUrl);
        return this;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getPhotoUrl() {
        return this.photoUrl;
    }

    public Utilisateur photoUrl(String photoUrl) {
        this.setPhotoUrl(photoUrl);
        return this;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Set<Campagne> getCampagnes() {
        return this.campagnes;
    }

    public void setCampagnes(Set<Campagne> campagnes) {
        if (this.campagnes != null) {
            this.campagnes.forEach(i -> i.setUtilisateur(null));
        }
        if (campagnes != null) {
            campagnes.forEach(i -> i.setUtilisateur(this));
        }
        this.campagnes = campagnes;
    }

    public Utilisateur campagnes(Set<Campagne> campagnes) {
        this.setCampagnes(campagnes);
        return this;
    }

    public Utilisateur addCampagne(Campagne campagne) {
        this.campagnes.add(campagne);
        campagne.setUtilisateur(this);
        return this;
    }

    public Utilisateur removeCampagne(Campagne campagne) {
        this.campagnes.remove(campagne);
        campagne.setUtilisateur(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Utilisateur)) {
            return false;
        }
        return id != null && id.equals(((Utilisateur) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Utilisateur{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", email='" + getEmail() + "'" +
            ", password='" + getPassword() + "'" +
            ", societe='" + getSociete() + "'" +
            ", presentation='" + getPresentation() + "'" +
            ", telephone=" + getTelephone() +
            ", videoUrl='" + getVideoUrl() + "'" +
            ", photoUrl='" + getPhotoUrl() + "'" +
            "}";
    }
}
